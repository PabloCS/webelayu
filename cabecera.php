
<div class="header_top_menu pt-2 pb-2 bg_color">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-sm-8">
				<div class="header_top_menu_address">
					<div class="header_top_menu_address_inner">
						<ul>
							<li><a href="mailto: info@elayudante.es"><i class="fas fa-envelope" target="_blank" style="margin-right: 10px; font-size: 1.2em"></i>info@elayudante.es</a></li>
							<li><a href="https://g.page/elayudantees?share"><i class="fas fa-map-marker" target="_blank" style="margin-right: 10px; font-size: 1.2em"></i>C/Floranes 23 entlo</a></li>
							<li><a href="tel:+34-942-40-85-70"><i class="fas fa-phone" target="_blank" style="margin-right: 10px; font-size: 1.2em"></i>+34 942 40 85 70</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-sm-4">
				<div class="header_top_menu_icon">
					<div class="header_top_menu_icon_inner">
						<ul>
							<li><a href="https://es-es.facebook.com/elayudantees/"><i class="fab fa-facebook-f"style="font-size:1.2em"></i></a></li>
							<li><a href="https://www.instagram.com/elayudantees/?hl=es"><i class="fab fa-instagram"style="font-size:1.2em"></i></a></li>
							<li><a href="https://www.linkedin.com/company/elayudante/"><i class="fab fa-linkedin"style="font-size:1.2em"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="sticky-header" class="techno_nav_manu d-md-none d-lg-block d-sm-none d-none">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo mt-4">
						<a class="logo_img" href="index.php" title="techno">
							<img class="logo" src="assets/images/logo2.png" alt="logo ayudante" />
						</a>
						<a class="main_sticky" href="index.php" title="techno">
							<img class="logo" src="assets/images/logo.png" alt="astute" />
						</a>
					</div>
				</div>
				<div class="col-md-9">
					<nav class="techno_menu">
						<ul class="nav_scroll">
							<li><a href="../webelayu/index.php">Inicio</a></li>
							<li><a href="../webelayu/about.php">Nosotros</a></li>
							<li><a href="../webelayu/servicios.php">Servicios</a>
								<ul class="sub-menu">
									<li><a href="../webelayu/servicios_dir.php">Dirección Ejecutiva</a></li>
									<li><a href="../webelayu/servicios_marketing.php">Marketing & Comunicación</a></li>
									<li><a href="../webelayu/servicios_tecno.php">Tecnología & Informática</a></li>
									<li><a href="../webelayu/servicios_artes.php">Artes Gráficas</a></li>
								</ul>
							</li>
							<li><a href="../webelayu/projects.php">Proyectos</a>
								<!--
								<ul class="sub-menu">
									<li><a href="../webelayu/project_uno.html">Proyecto Uno</a></li>
									<li><a href="../webelayu/project_dos.html">Proyecto Dos</a></li>
									<li><a href="../webelayu/project_trers.html">Proyecto Tres</a></li>
									<li><a href="../webelayu/project_cuatro.html">Proyecto Cuatro</a></li>
									<li><a href="../webelayu/project_cinco.html">Proyecto Cinco</a></li>
									<li><a href="../webelayu/project_seis.html">Proyecto Seis</a></li>
								</ul>
								-->
							</li>
							<li><a href="../webelayu/blog.php">Blog </a></li>
							<li><a href="../webelayu/contacto.php">Contacto</a></li>
						</ul>				
					</nav>
				</div>
			</div>
		</div>
	</div>
